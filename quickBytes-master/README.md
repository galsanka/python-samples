# quickBytes

## Project Dependencies
- Python
- Tkinter (which the student machines have)
- Environment: XMing/XQuartz/Comes with Linux

## Setup and Install the Code
Download the files from the [repository](https://gitlab.com/Hageman/quickBytes/ "quickBytes")


## Run and Use the Code
`python program.py input.json` </br>

input.json must be a file containing the restaurants and their properties in json format. </br>

e.g. `python program.py data1.json`

## Testing
`make test-output` </br>
Runs a `diff` that compares different outputs to those from known inputs

## Benchmarking
`make test-memory` </br>
Runs `time` with the memory flag on inputs of size 10, 100, 500 </br>

#### Results

| Input Size | Memory Usage (kb) |
|------------|-------------------|
| 10         | 19240             |
| 100        | 19384             |
| 500        | 20176             |