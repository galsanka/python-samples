import sys
import os
import random

n = int(sys.argv[1]) # number of restaurants

print("[")

for i in range(0, n):
    print("  {")
    name = str(i)
    print("    \"name\": \"{}\",".format(name))
    print("    \"properties\": {")
    print("      \"coordinates\": [{}, {}],".format(random.randint(1, 101), random.randint(1, 101)))
    print("      \"price\": {},".format(random.randint(1,4)))
    print("      \"openingHours\": [{}, {}],".format(random.randint(0, 12), random.randint(13, 24)))
    s = random.randint(1,4)
    if s == 1:
        print("      \"style\": [\"{}\"]".format("snack"))
    elif s == 2:
        print("      \"style\": [\"{}\"]".format("dinner"))
    else:
        print("      \"style\": [\"{}\", \"{}\"]".format("snack","dinner"))
    print("    }")
    if i < n-1:
        print("  },")
    else:
        print("  }")

print("]")
