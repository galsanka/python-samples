#!/usr/bin/env python3

import math

# Class declaration
class Restaurant:
    "Contains: Name, Coordinates, PriceRange, OpeningHours"
    name = "Subway"
    xcoordinate = 0 # Cartesian coordinates
    ycoordinate = 0
    price = 1 # 1 is cheapest, 3 is most expensive 
    openingHour = 0 # Military time
    closingHour = 23
    style = set()

    def distance(self, currentx, currenty):
        return math.sqrt(math.pow((self.xcoordinate - currentx),2) + math.pow((self.ycoordinate - currenty),2))

    def preference(self, priceWeight, currentx, currenty):
        return float(priceWeight * self.price) + ((1 - priceWeight) * float(self.distance(currentx, currenty)))

class Heap(object):
    def __init__(self, heap=None):
        self.heap = [] if heap is None else heap

    def inv_heapify(self, child_index, pw, xc, yc):
        # reheapify starting from bottom to root
        heap = self.heap
        child = child_index
        while child > 0:
            parent = child // 2
            if self.compare(heap[parent], heap[child], pw, xc, yc):
                return
            heap[parent], heap[child] = heap[child], heap[parent]
            child = parent

    def heapify(self, parent_index, pw, xc, yc):
        # reheapify starting from root
        heap = self.heap
        length = len(heap)
        for root in range(length//2-1, -1, -1):
            root_val = heap[root]
            child = 2*root+1
            while child < length:
                if child+1 < length and self.compare(heap[child + 1], heap[child], pw, xc, yc):
                    child +=1
                if self.compare(root_val, heap[child], pw, xc, yc):
                    break
                heap[child], heap[(child-1)//2] = heap[(child-1)//2], heap[child]
                child = child *2 +1

    def del_min(self, pw, xc, yc):
        heap = self.heap
        last_element = heap.pop()
        if not heap:
            return last_element
        item = heap[0]
        heap[0] = last_element
        for i in range(0, len(heap)):
            self.heapify(i, pw, xc, yc)
        return item

    def min(self):
        if not self.heap:
            return None
        return self.heap[0]

    def add(self, priceWeight, xcoord, ycoord, element):
        self.heap.append(element)
        self.inv_heapify(len(self.heap) - 1, priceWeight, xcoord, ycoord)

    def compare(self, r1, r2, priceWeight, xcoord, ycoord):
        if r1.preference(priceWeight, xcoord, ycoord) < r2.preference(priceWeight, xcoord, ycoord):
            return True
        else:
            return False

    def printHeap(self, pw, xc, yc):
        h = self.min()
        print("{}".format(h.name))
        self.del_min(pw, xc, yc)
        if len(self.heap) > 0:
            self.printHeap(pw, xc, yc)
        else:
            return

