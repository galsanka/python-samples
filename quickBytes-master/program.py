#!/usr/bin/env python3

from tkinter import *
from tkinter import ttk
import os
from priority_queue import Restaurant
from priority_queue import Heap
import sys
import json

from PIL import Image, ImageTk

def load(rests, priceWeight, xCoord, yCoord):
    if (len(sys.argv) == 1):
        print("Please enter a data file")
        return
    with open(sys.argv[1]) as json_data:
        data = json.load(json_data)

    for rest in data:
        temp = Restaurant()
        temp.name = rest["name"]
        temp.xcoordinate = rest["properties"]["coordinates"][0]
        temp.ycoordinate = rest["properties"]["coordinates"][1]
        temp.price = rest["properties"]["price"]
        temp.openingHour = rest["properties"]["openingHours"][0]
        temp.closingHour = rest["properties"]["openingHours"][1]
        for s in rest["properties"]["style"]:
            temp.style.add(s)
        rests.add(priceWeight, xCoord, yCoord, temp)

class GUI:
    def __init__(self, master):
        self.master = master
        master.title("QuickBytes")

        
        self.label = Label(master, text="Select your preference for price vs distance")
        self.label.pack()
        v = IntVar()
        self.slider = Scale(from_=0, to=100, length=300, orient=HORIZONTAL, variable = v, label = "Price                                                    Distance")
        self.slider.set(50)
        self.slider.pack()

        v1 = IntVar()
        self.slider1 = Scale(from_=0, to=100, length=300, orient=HORIZONTAL, variable = v1, label = "Longitude")
        self.slider1.set(50)
        self.slider1.pack()

        v2 = IntVar()
        self.slider2 = Scale(from_=0, to=100, length=300, orient=HORIZONTAL, variable = v2, label = "Latitude")
        self.slider2.set(50)
        self.slider2.pack()

        self.button = Button(master, command=self.eat, text="Eat")
        self.button.pack()

    def eat(self):
        os.system('clear')
        priceWeight = self.slider.get()
        xc = self.slider1.get()
        yc = self.slider.get()
        PQ = Heap()
        load(PQ, float(priceWeight/100), xc, yc)
        for i in range(0,6): PQ.heapify(i, priceWeight, xc, yc)
        PQ.printHeap(priceWeight, xc, yc)
        os.popen("clear")
    

root = Tk()
my_gui = GUI(root)
root.mainloop()
