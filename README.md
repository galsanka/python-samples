# hulk.py
Tries to crack a SHA1 password using permutation. Uses Python iterators

# Activities 01 and 02.ipynb
Some introduction to data visualization on Jupyter Notebooks

# quickBytes
Project using Python for data structures class. Main feature is a heap 
implementation using python. Done with a team of 3.

# Sirach
Bot that tweets out periodically verses from the Book of 
Sirach/Ecclesiastes. Uses Python (tweetpy) and deployed on heroku.
